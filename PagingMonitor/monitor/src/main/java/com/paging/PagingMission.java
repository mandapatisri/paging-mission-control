package com.paging;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author Srilatha Mandapati
 * 
 *         This class is the main processing for alerts
 *
 */
public class PagingMission {
    private List<String> studyIdList = new ArrayList<String>();

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) {
        PagingMission aMissionControl = new PagingMission();
        List<Reading> studiesList = null;
        if (0 < args.length) {
            try {
                studiesList = aMissionControl.getStudyList(args[0]);
            } catch (Exception e) {
                System.err.println("Unable to read the input file:" + args[0]);
                System.exit(0);
            }
            aMissionControl.processStudyList(studiesList);
        } else {
            System.err.println("File name is required:" + args.length);
            System.exit(0);
        }
    }

    /**
     * Processes the input studies list
     * 
     * @param studiesList
     */
    private void processStudyList(List<Reading> studiesList) {

        Map<String, List<Reading>> readings = new HashMap<String, List<Reading>>();
        Collections.sort(studyIdList);

        // Subdivide into the study id buckets
        for (Reading study : studiesList) {
            if (readings.get(study.getSatelliteId() + study.getComponentId()) != null) {
                List<Reading> currentList = readings.get(study.getSatelliteId() + study.getComponentId());
                currentList.add(study);
            } else {
                List<Reading> newList = new ArrayList<Reading>();
                newList.add(study);
                readings.put(study.getSatelliteId() + study.getComponentId(), newList);
            }
        }

        List<Alert> alertList = new ArrayList<Alert>();

        // Process each set now
        for (String aID : studyIdList) {
            List<Reading> processList = readings.get(aID);
            Collections.sort(processList,
                (reading1, reading2) -> reading1.getTimeStampDate().compareTo(reading2.getTimeStampDate()));

            for (int i = 0; i < processList.size() - 2; i++) {

                Reading currentReading = processList.get(i);
                boolean isHigh = checkHighReadings(currentReading, processList.subList(i + 1, processList.size()));
                if (isHigh) {
                    alertList.add(new Alert(currentReading.getSatelliteId(), "RED HIGH",
                        currentReading.getComponentId(), getFormattedDateString(currentReading.getTimeStampDate())));
                }

                boolean isLow = checkLowReadings(currentReading, processList.subList(i + 1, processList.size()));
                if (isLow) {
                    alertList.add(new Alert(currentReading.getSatelliteId(), "RED LOW", currentReading.getComponentId(),
                        getFormattedDateString(currentReading.getTimeStampDate())));
                }
            }
        }
        printAsJson(alertList);
    }

    /**
     * Method to return in UTC format
     * 
     * @param inDate
     * @return
     */

    private String getFormattedDateString(Timestamp inDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return simpleDateFormat.format(inDate);
    }

    /**
     * 
     * @param currentReading
     * @param subList
     * @return
     */
    private boolean checkHighReadings(Reading currentReading, List<Reading> subList) {
        int count = 0;

        // Check High reading for Thermostat only
        if ("TSTAT".equals(currentReading.getComponentId())) {
            long milliseconds1 =
                currentReading.getTimeStampDate().getTime() - currentReading.getTimeStampDate().getTime();
            int seconds1 = (int) milliseconds1 / 1000;

            for (Reading nextReading : subList) {
                long milliseconds2 =
                    nextReading.getTimeStampDate().getTime() - currentReading.getTimeStampDate().getTime();
                int seconds2 = (int) milliseconds2 / 1000;

                if (seconds1 < 300 && seconds2 < 300) {
                    if (currentReading.getRawvalue() > currentReading.getRedHighLimit()
                        && nextReading.getRawvalue() > nextReading.getRedHighLimit()) {
                        count++;
                    }
                }
            }
        }
        return count >= 2 ? true : false;
    }

    /**
     * 
     * @param currentReading
     * @param subList
     * @return
     */
    private boolean checkLowReadings(Reading currentReading, List<Reading> subList) {
        int count = 0;

        // Check Low reading for Battery only
        if ("BATT".equals(currentReading.getComponentId())) {
            long milliseconds1 =
                currentReading.getTimeStampDate().getTime() - currentReading.getTimeStampDate().getTime();
            int seconds1 = (int) milliseconds1 / 1000;

            for (Reading nextReading : subList) {
                long milliseconds2 =
                    nextReading.getTimeStampDate().getTime() - currentReading.getTimeStampDate().getTime();
                int seconds2 = (int) milliseconds2 / 1000;

                if (seconds1 < 300 && seconds2 < 300) {
                    if (currentReading.getRawvalue() < currentReading.getRedLowLimit()
                        && nextReading.getRawvalue() < nextReading.getRedLowLimit()) {
                        count++;
                    }
                }
            }
        }
        return count >= 2 ? true : false;
    }

    /**
     * 
     * @param filename
     * @return
     * @throws IOException
     */
    private List<Reading> getStudyList(String filename) throws IOException {
        FileInputStream fiStream = new FileInputStream(filename);
        BufferedReader bReader = new BufferedReader(new InputStreamReader(fiStream));

        String aLine;
        List<Reading> studiesList = new ArrayList<Reading>();

        while ((aLine = bReader.readLine()) != null) {
            Reading aReading = new Reading(aLine);
            studiesList.add(aReading);
            if (!studyIdList.contains(aReading.getSatelliteId() + aReading.getComponentId())) {
                studyIdList.add(aReading.getSatelliteId() + aReading.getComponentId());
            }
        }
        fiStream.close();
        return studiesList;
    }

    /**
     * Prints JSON file output to standard out
     * 
     * @param alertList
     */
    private void printAsJson(List<Alert> alertList) {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(alertList);
        System.out.println(json);
    }
}