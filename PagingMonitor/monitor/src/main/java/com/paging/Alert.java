package com.paging;

/**
 * 
 * @author Srilatha Mandapati
 * 
 *         POJO Alert
 *
 */
public class Alert {
    // Satellite ID
    private String satelliteId;

    // Severity Level
    private String severity;

    // Component is battery or thermostat
    private String component;

    // Timestamp
    private String timestamp;

    public Alert(String satelliteId, String severity, String component, String timestamp) {
        super();
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}