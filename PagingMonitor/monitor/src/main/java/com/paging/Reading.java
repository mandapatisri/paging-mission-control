package com.paging;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * 
 * @author Srilatha Mandapati
 *
 *         POJO for input reading fields
 */
public class Reading {

    private java.sql.Timestamp timeStampDate;
    private String satelliteId;
    private Double redHighLimit;
    private Double redLowLimit;
    private Double yellowHighLimit;
    private Double yellowLowLimit;
    private Double rawvalue;
    private String componentId;

    public Reading(String studyId) {
        StringTokenizer tokenizer = new StringTokenizer(studyId, "|");

        String timeStamp = tokenizer.nextToken();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        Date date = null;
        try {
            date = formatter.parse(timeStamp);
        } catch (ParseException e) {
            System.exit(1); // Not expected error handling
        }
        timeStampDate = new Timestamp(date.getTime());

        satelliteId = tokenizer.nextToken();
        redHighLimit = Double.valueOf(tokenizer.nextToken());
        yellowHighLimit = Double.valueOf(tokenizer.nextToken());
        yellowLowLimit = Double.valueOf(tokenizer.nextToken());
        redLowLimit = Double.valueOf(tokenizer.nextToken());
        rawvalue = Double.valueOf(tokenizer.nextToken());
        componentId = tokenizer.nextToken();

    }

    public java.sql.Timestamp getTimeStampDate() {
        return timeStampDate;
    }

    public void setTimeStampDate(java.sql.Timestamp timeStampDate) {
        this.timeStampDate = timeStampDate;
    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public Double getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(Double redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public Double getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(Double redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public Double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(Double yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public Double getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(Double yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public Double getRawvalue() {
        return rawvalue;
    }

    public void setRawvalue(Double rawvalue) {
        this.rawvalue = rawvalue;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    @Override
    public String toString() {
        return "Reading [timeStampDate=" + timeStampDate + ", satelliteId=" + satelliteId + ", redHighLimit="
            + redHighLimit + ", redLowLimit=" + redLowLimit + ", yellowHighLimit=" + yellowHighLimit
            + ", yellowLowLimit=" + yellowLowLimit + ", rawvalue=" + rawvalue + ", componentId=" + componentId + "]";
    }
}