# Monitor Project
Create the alerting mechanism for the readings

## Input
Provide the name of the file as a first argument

# Validation
The README file of the project said no validation of the input file, file format and fields required. No validation performed

# Exception handling
All the exceptions are sent to the caller. Since no error handling is done, no exception are handled locally

#Execution Example
mvn compile exec:java -Dexec.mainClass="com.paging.PagingMission" -Dexec.args="readings.txt"

Modify the readings.txt with valid input values as needed
